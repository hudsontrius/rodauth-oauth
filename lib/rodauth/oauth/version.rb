# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "0.10.1"
  end
end
